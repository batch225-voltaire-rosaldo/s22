// Array Methods

// Mutator Methods

/*
	-Mutator methods are functions that "mutate" or change an array after they're created.
	- These methods manipulate the original array performing various task such as adding and removing element.
*/

// push()

/*
	- Adds an element in the end of an array and return the array's length.

	Syntax:
		arrayName.push();
*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

console.log('Current array:');
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method:');
console.log(fruits);

// Adding multiple elements to an array.

fruits.push('Avocado', 'Guava');
console.log(fruits);

// pop()
/*
	- Removes the last element in an array and returns the removed element.

	- Syntax:
		arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method: ');
console.log(fruits);

// unshift()

/*
	- adds one or more elements at the beginning of an array.
	Syntax:

		arrayName.unshift('elementA');
		arrayName.unshift('elementA', 'elementB');
*/

fruits.unshift('Lime', 'Banana');
console.log(fruits);

// shift()

 // - Removes an element at the beginning of an array and returns the removed element

 // - Syntax:
		// arrayName.shift();

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log(fruits);

// splice()

// - simultaneously removes elements from a specified index number and adds element

	//  Syntax:
	//  arrayName.splice(startingIndex, deleteCount, elementstobeadded)

fruits.splice(1,2);
console.log(fruits);
let fruitsIndex = fruits.length;
console.log(fruitsIndex);
fruits.splice(0,fruitsIndex);
console.log(fruits);


// sort()x
	// Rearrange the array element in alphanumeric order
	// - Syntax:
		// arrayName.sort();

const randomThings = ['Cat', 'Boy', 'goat', 'jack', 'black', 'apps', 'zoo' ];
randomThings.sort();
console.log(randomThings);


// reverse()

	// - Reverse the order of array elements
	 // Syntax:
		// arrayName.reverse()

randomThings.reverse();
console.log(randomThings);

//============================
//// Non-Mutator Methods

// indexOf()
/*
	-Returns the index number of the first matching element found in an array.
	-if no match was found, the ersult will be -1
	- the search process will be done from first element proceeding to the element.

	-Syntax:
		arrayName.indexOf(searchValue)
*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'BR', 'FR', 'DE'];

let firstIndex = countries.indexOf('CAN');
console.log(firstIndex);

// slice()

	// - Portion/slice elements from an array and returns a new array
	// - Syntax:
		// arrayName.slice(starttingIndex);
		// arrayName.slice(starttingIndex, endingIndex);

// Slicing off elements from a specified index to the first element
let sliceArrayA = countries.slice(4);
console.log(sliceArrayA);
console.log(countries);

let sliceArrayB = countries.slice(2,4);
console.log(sliceArrayB);

// Slicing off elements starting from the last element of an array

let sliceArrayC = countries.slice(-3);
console.log(sliceArrayC);

// forEach()

	// - Similar to a for loop that itterates on each array element.
	// - for each item in the array, the anonymous function passed in forEach() method will be run.

	// Syntax:
		// arrayName.forEach(function(indivElement){
		// 	statement
		// })

countries.forEach(function(country){
	console.log(country);
});

// includes()

// includes() - includes method checks if the argument passed can be found in the array.

// - it returns a boolean which can be saved in a variable.
// - return true if the argument is found in the array
// - return false if it is not in the array.

// Syntax:
	// arrayName.includes(<argumentToFind>)

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");

console.log(productFound1);//returns true

let productFound2 = products.includes("Headset");

console.log(productFound2);//returns false













































































